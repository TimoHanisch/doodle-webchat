package de.timoh.doodle.webchat.model;

import de.timoh.doodle.webchat.model.dto.MessageDTO;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Timo Hanisch
 */
@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String author;

    @Lob
    private String message;

    @Column(name = "date_created")
    private LocalDateTime date;

    public Message() {
        super();
    }

    public Message(String author, String message, LocalDateTime date) {
        this.author = author;
        this.message = message;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Message message = (Message) o;
        return id == message.id;
    }

    public static Message fromDTO(MessageDTO messageDTO) {
        return new Message(messageDTO.getAuthor(), messageDTO.getMessage(), LocalDateTime.now());
    }
}
