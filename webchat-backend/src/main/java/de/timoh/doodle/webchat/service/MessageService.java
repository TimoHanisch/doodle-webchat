package de.timoh.doodle.webchat.service;

import de.timoh.doodle.webchat.model.Message;
import de.timoh.doodle.webchat.model.dto.MessageDTO;
import de.timoh.doodle.webchat.repository.MessageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Timo Hanisch
 */
@Service
public class MessageService {

    private final MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Message createMessage(MessageDTO messageDTO) {
        return messageRepository.save(Message.fromDTO(messageDTO));
    }

    public List<Message> getMessages() {
        return messageRepository.findAllByOrderByDateAsc();
    }
}
