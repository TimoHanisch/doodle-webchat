package de.timoh.doodle.webchat.repository;

import de.timoh.doodle.webchat.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Timo Hanisch
 */
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByOrderByDateAsc();
}
