package de.timoh.doodle.webchat.controller;

import de.timoh.doodle.webchat.model.Message;
import de.timoh.doodle.webchat.model.dto.MessageDTO;
import de.timoh.doodle.webchat.service.MessageService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * @author Timo Hanisch
 */
@Controller
public class WebChatController {

    private final MessageService messageService;

    public WebChatController(MessageService messageService) {
        this.messageService = messageService;
    }

    @MessageMapping("/message")
    @SendTo("/topic/chat")
    public Message sendMessage(MessageDTO messageDTO) {
        return messageService.createMessage(messageDTO);
    }
}
