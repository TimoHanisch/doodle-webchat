package de.timoh.doodle.webchat.controller;

import de.timoh.doodle.webchat.model.Message;
import de.timoh.doodle.webchat.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MessageService service;

    @Test
    public void shouldOnlyAllowGET() throws Exception {
        mvc.perform(post("/messages").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void shouldRenderMessagesJson() throws Exception {
        LocalDateTime date = LocalDateTime.now();
        List<Message> messages = new ArrayList<>();
        messages.add(new Message("Test", "Test message", date));

        given(service.getMessages()).willReturn(messages);

        mvc.perform(get("/messages").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$[0].author", is("Test")))
                .andExpect(jsonPath("$[0].message", is("Test message"))).andExpect(jsonPath("$[0].date", is(date.toString())));
    }
}
