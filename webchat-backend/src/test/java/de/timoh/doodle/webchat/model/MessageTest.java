package de.timoh.doodle.webchat.model;

import de.timoh.doodle.webchat.model.dto.MessageDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created on 28.05.2018.
 *
 * @author Timo Hanisch
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageTest {

    @Test
    public void shouldReturnProperMessageFromDTO() {
        String author = "Test author";
        String messageText = "Hello World";
        MessageDTO messageDTO = new MessageDTO(author, messageText);
        Message message = Message.fromDTO(messageDTO);

        assertEquals(author, message.getAuthor());
        assertEquals(messageText, message.getMessage());
    }

    @Test
    public void shouldOnlyBeEqualForSameIdsAndSameObject() {
        Message message1 = new Message("test", "test", LocalDateTime.now());
        message1.setId(1);
        Message message2 = new Message("test2", "test2", LocalDateTime.now());
        message2.setId(1);
        Message message3 = new Message("test3", "test3", LocalDateTime.now());
        message3.setId(100);

        assertEquals(message1, message1);
        assertEquals(message1, message2);
        assertNotEquals(message1, message3);
    }
}
