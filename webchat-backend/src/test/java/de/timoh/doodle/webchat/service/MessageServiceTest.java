package de.timoh.doodle.webchat.service;

import de.timoh.doodle.webchat.model.Message;
import de.timoh.doodle.webchat.model.dto.MessageDTO;
import de.timoh.doodle.webchat.repository.MessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 28.05.2018.
 *
 * @author Timo Hanisch
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MessageServiceTest {

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageRepository messageRepository;

    @Test
    public void shouldCreateMessageFromDTO() {
        MessageDTO msgDTO = new MessageDTO("Test author", "Test message");
        Message message = messageService.createMessage(msgDTO);

        assertNotNull(message);
        assertNotNull(messageRepository.findById(message.getId()));
    }

    @Test
    public void shouldGetListOfMessagesOrderedByDate() {
        Message message1 = messageRepository.save(new Message("Author1", "message1", LocalDateTime.now()));
        Message message2 = messageRepository.save(new Message("Author2", "message2", LocalDateTime.now().plusDays(1)));
        Message message3 = messageRepository.save(new Message("Author3", "message3", LocalDateTime.now().minusHours(5)));
        List<Message> messageList = asList(message3, message1, message2);
        List<Message> resultList = messageService.getMessages();
        assertEquals(messageList, resultList);
    }
}
