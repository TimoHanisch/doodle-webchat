package de.timoh.doodle.webchat.controller;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.timoh.doodle.webchat.model.Message;
import de.timoh.doodle.webchat.model.dto.MessageDTO;
import de.timoh.doodle.webchat.repository.MessageRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebChatControllerTest {
    @Value("${local.server.port}")
    private int    port;
    private String URL;

    private static final String MESSAGE_ENDPOINT = "/topic/chat";
    private static final String CHAT_ENDPOINT    = "/app/message";

    private CompletableFuture<Message> completableFuture;

    private WebSocketStompClient stompClient;

    @Autowired
    private MessageRepository messageRepository;

    @Before
    public void setup() {
        completableFuture = new CompletableFuture<>();
        URL = "ws://localhost:" + port + "/webchat";
        stompClient = new WebSocketStompClient(new SockJsClient(Collections.singletonList(new WebSocketTransport(new StandardWebSocketClient()))));
        // Since the Jackson2MessageConverter cannot handle LocalDateTime, we have to register an object mapper
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.getObjectMapper().registerModule(new JavaTimeModule());
        stompClient.setMessageConverter(converter);
    }

    @Test
    public void shouldEchoIncomingMessage() throws URISyntaxException, InterruptedException, ExecutionException, TimeoutException {
        Message message = initializeMessage();
        assertNotNull(message);
    }

    @Test
    public void shouldSaveMessageToDB() throws URISyntaxException, InterruptedException, ExecutionException, TimeoutException {
        Message message = initializeMessage();
        assertNotNull(messageRepository.findById(message.getId()));
    }

    private Message initializeMessage() throws InterruptedException, ExecutionException, TimeoutException {
        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        stompSession.subscribe(MESSAGE_ENDPOINT, new MessageFrameHandler());
        stompSession.send(CHAT_ENDPOINT, new MessageDTO("Test", "test message"));
        return completableFuture.get(10, SECONDS);
    }

    private class MessageFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return Message.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            completableFuture.complete((Message) o);
        }
    }
}
