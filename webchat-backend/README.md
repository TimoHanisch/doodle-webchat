# Backend

Don't use this for production! Has very open CORS configs.

Offers two(three) API endpoints

## REST

`GET /messages` => Returns an ordered list of all created messages (Not paginated)

## WebSocket

Basic WebSocket endpoint can be found under `/webchat`

`/app/message` => Endpoint where new messages are send to

`/topic/chat` => Topic for broadcasting new messages
