import PropTypes from 'prop-types';

export const MessagePropTypes = PropTypes.shape({
    author: PropTypes.string,
    message: PropTypes.string,
    date: PropTypes.string
});
