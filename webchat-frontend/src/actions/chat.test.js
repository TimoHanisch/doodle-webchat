import { fetchMessages, updateAuthor } from './chat';

describe('Test chat actions', () => {
    test('fetchMessages should call dispatch with MESSAGES_LOADING', () => {
        global.fetch = jest.fn(() => {
            return new Promise((resolve, reject) => {
                resolve({
                    ok: true,
                    json: () => []
                });
            });
        });
        const callback = jest.fn();
        const dispatchMock = jest.fn();
        const resultFunc = fetchMessages(callback);

        resultFunc(dispatchMock);

        expect(dispatchMock).toHaveBeenCalledTimes(1);
        expect(dispatchMock).toHaveBeenCalledWith({
            type: 'MESSAGES_LOADING',
            messagesLoading: true
        });
    });

    test('fetchMessages should dispatch an error if api call fails', () => {
        global.fetch = jest.fn(() => {
            return new Promise((resolve, reject) => {
                resolve({
                    ok: false,
                    json: () => []
                });
            });
        });
        const callback = jest.fn();
        const dispatchMock = jest.fn();
        const resultFunc = fetchMessages(callback);

        const result = resultFunc(dispatchMock).then(() => {
            expect(dispatchMock).toHaveBeenCalledTimes(2);
            expect(dispatchMock.mock.calls[1][0]).toEqual({
                type: 'MESSAGES_ERROR',
                messagesError: true
            });
        });
    });

    test('fetchMessages should dispatch MESSAGES_LOADING if messages were fetched', () => {
        global.fetch = jest.fn(() => {
            return new Promise((resolve, reject) => {
                resolve({
                    ok: true,
                    json: () => []
                });
            });
        });
        const callback = jest.fn();
        const dispatchMock = jest.fn();
        const resultFunc = fetchMessages(callback);

        const result = resultFunc(dispatchMock).then(() => {
            expect(dispatchMock).toHaveBeenCalledTimes(3);
            expect(dispatchMock.mock.calls[1][0]).toEqual({
                type: 'MESSAGES_LOADING',
                messagesLoading: false
            });
            expect(dispatchMock.mock.calls[2][0]).toEqual({
                type: 'MESSAGES_FETCHED',
                messages: []
            });
            expect(callback).toHaveBeenCalledTimes(1);
        });
    });

    test('updateAuthor should dispatch AUTHOR_SET', () => {
        const author = 'Test';
        const dispatchMock = jest.fn();
        const result = updateAuthor(author);

        result(dispatchMock);

        expect(dispatchMock).toHaveBeenCalledWith({
            type: 'AUTHOR_SET',
            author
        });
    });
});
