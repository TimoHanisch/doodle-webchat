export default {
    connecting(isConnecting) {
        return {
            type: 'WS_CONNECTING',
            isConnecting
        };
    },
    connected(isConnected) {
        return {
            type: 'WS_CONNECTED',
            isConnected
        };
    },
    connect() {
        return {
            type: 'WS_CONNECT'
        };
    },
    disconnect() {
        return {
            type: 'WS_DISCONNECT'
        };
    },
    sendMessage(message) {
        return {
            type: 'WS_SEND_MESSAGE',
            message
        };
    },
    addMessage(message) {
        return {
            type: 'WS_ADD_MESSAGE',
            message
        };
    }
};
