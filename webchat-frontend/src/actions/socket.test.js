import actions from './socket';

describe('Test socket actions', () => {
    test('connecting should return WS_CONNECTING', () => {
        const result = actions.connecting(true);

        expect(result).toEqual({
            type: 'WS_CONNECTING',
            isConnecting: true
        });
    });

    test('connected should return WS_CONNECTED', () => {
        const result = actions.connected(true);

        expect(result).toEqual({
            type: 'WS_CONNECTED',
            isConnected: true
        });
    });

    test('connect should return WS_CONNECT', () => {
        const result = actions.connect();

        expect(result).toEqual({
            type: 'WS_CONNECT'
        });
    });

    test('disconnect should return WS_DISCONNECT', () => {
        const result = actions.disconnect();

        expect(result).toEqual({
            type: 'WS_DISCONNECT'
        });
    });

    test('sendMessage should return WS_SEND_MESSAGE', () => {
        const message = 'Test message';
        const result = actions.sendMessage(message);

        expect(result).toEqual({
            type: 'WS_SEND_MESSAGE',
            message
        });
    });

    test('addMessage should return WS_ADD_MESSAGE', () => {
        const message = 'Add message';
        const result = actions.addMessage(message);

        expect(result).toEqual({
            type: 'WS_ADD_MESSAGE',
            message
        });
    });
});
