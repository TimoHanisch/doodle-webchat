const GET_MESSAGES_ENDPOINT = 'http://localhost:8080/messages';

/**
 *
 * @param {boolean} messagesLoading
 */
function messagesLoading(messagesLoading) {
    return {
        type: 'MESSAGES_LOADING',
        messagesLoading
    };
}

/**
 *
 * @param {boolean} messagesError
 */
function messagesError(messagesError) {
    return {
        type: 'MESSAGES_ERROR',
        messagesError
    };
}

/**
 *
 * @param {Array.<Message>} messages
 */
function messagesFetched(messages) {
    return {
        type: 'MESSAGES_FETCHED',
        messages
    };
}

function setAuthor(author) {
    return {
        type: 'AUTHOR_SET',
        author
    };
}

export function fetchMessages(callback) {
    return dispatch => {
        // Update loading state
        dispatch(messagesLoading(true));

        // Call the API to fetch messages
        return fetch(GET_MESSAGES_ENDPOINT)
            .then(response => {
                // If something went wrong, lets throw an error
                if (!response.ok) {
                    throw new Error(response.statusText);
                }
                // If everything worked out, update the loading state
                // and pass the JSON down the promise chain
                dispatch(messagesLoading(false));
                return response.json();
            })
            .then(messages => {
                dispatch(messagesFetched(messages));
                callback();
            })
            .catch(e => {
                console.error(e);
                // Something went wrong, update the error state
                dispatch(messagesError(true));
            });
    };
}

export function updateAuthor(author) {
    return dispatch => {
        dispatch(setAuthor(author));
    };
}
