import React from 'react';
import { shallow } from 'enzyme';
import Chat from './Chat';

describe('Test Chat', () => {
    test('should render ChatList and ChatInpout with correct props', () => {
        const localAuthor = 'Test';
        const messages = [];
        const sendMessage = jest.fn();
        const wrapper = shallow(
            <Chat
                localAuthor={localAuthor}
                messages={messages}
                sendMessage={sendMessage}
            />
        );

        const chatList = wrapper.childAt(0);
        expect(chatList.is('ChatList')).toBeTruthy();
        expect(chatList.props().messages).toBe(messages);
        expect(chatList.props().localAuthor).toBe(localAuthor);
        const chatInput = wrapper.childAt(1);
        expect(chatInput.is('ChatInput')).toBeTruthy();
        expect(chatInput.props().sendMessage).toBe(sendMessage);
        expect(chatInput.props().localAuthor).toBe(localAuthor);
    });
});
