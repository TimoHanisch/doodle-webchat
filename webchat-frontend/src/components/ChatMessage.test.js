import React from 'react';
import { shallow } from 'enzyme';
import ChatMessage from './ChatMessage';

describe('Test ChatMessage', () => {
    test('should render a header and the message itself', () => {
        const author = 'Test';
        const message = {
            id: 0,
            author: 'Peter',
            message: 'Hey',
            date: new Date().toLocaleDateString()
        };
        const wrapper = shallow(
            <ChatMessage localAuthor={author} message={message} />
        );

        expect(wrapper.childAt(0).is('ChatMessageHeader')).toBeTruthy();
        expect(wrapper.childAt(0).props().author).toBe(message.author);
        expect(wrapper.childAt(0).props().date).toBe(message.date);
        expect(wrapper.childAt(1).text()).toBe(message.message);
    });
});
