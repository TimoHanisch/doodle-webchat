import React from 'react';
import { shallow } from 'enzyme';
import ChatMessageHeader from './ChatMessageHeader';

describe('Test ChatMessageHeader', () => {
    test('should render the author and the date', () => {
        const author = 'Test';
        const date = new Date().toLocaleDateString();
        const wrapper = shallow(
            <ChatMessageHeader author={author} date={date} />
        );

        expect(wrapper.childAt(0).text()).toBe(`${author} `);
        expect(wrapper.childAt(1).text()).toBe(
            new Date(date).toLocaleTimeString()
        );
    });
});
