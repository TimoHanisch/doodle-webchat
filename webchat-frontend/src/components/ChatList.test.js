import React from 'react';
import { shallow } from 'enzyme';
import ChatList from './ChatList';

describe('Test ChatList', () => {
    test('should render no messages if messages array is empty', () => {
        const messages = [];
        const author = 'Test';
        const wrapper = shallow(
            <ChatList localAuthor={author} messages={messages} />
        );

        expect(wrapper.find('ChatMessage')).toHaveLength(0);
    });

    test('should render a ChatMessage for every entry in the messages array', () => {
        const messages = [
            {
                id: 0,
                author: '1',
                message: 'test',
                date: new Date().toLocaleDateString()
            },
            {
                id: 1,
                author: '2',
                message: 'test2',
                date: new Date().toLocaleDateString()
            },
            {
                id: 2,
                author: '3',
                message: 'test3',
                date: new Date().toLocaleDateString()
            }
        ];
        const author = 'Test';
        const wrapper = shallow(
            <ChatList localAuthor={author} messages={messages} />
        );

        expect(wrapper.find('ChatMessage')).toHaveLength(messages.length);
    });
});
