import React from 'react';
import PropTypes from 'prop-types';
import ChatMessage from './ChatMessage';
import { MessagePropTypes } from '../constants/propTypes';

export default class ChatList extends React.PureComponent {
    static propTypes = {
        localAuthor: PropTypes.string.isRequired,
        messages: PropTypes.arrayOf(MessagePropTypes).isRequired
    };

    static STYLES = {
        container: {
            maxHeight: 'calc(100vh - 78px)', // Height of the bottom bar
            overflow: 'hidden',
            overflowY: 'auto'
        }
    };

    componentDidUpdate() {
        this._scroll();
    }

    componentDidMount() {
        this._scroll();
    }

    _scroll() {
        if (this.list) {
            this.list.scrollTop = this.list.scrollHeight;
        }
    }

    render() {
        const { localAuthor, messages } = this.props;
        return (
            <div
                ref={ref => {
                    this.list = ref;
                }}
                style={ChatList.STYLES.container}
            >
                {messages.map(message => (
                    <ChatMessage
                        key={message.id}
                        message={message}
                        localAuthor={localAuthor}
                    />
                ))}
            </div>
        );
    }
}
