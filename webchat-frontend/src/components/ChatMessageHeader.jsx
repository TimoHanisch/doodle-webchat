import React from 'react';
import PropTypes from 'prop-types';

const STYLES = {
    author: { fontWeight: 500 },
    date: { fontSize: 12 },
    container: { marginBottom: 4 }
};

const ChatMessageHeader = ({ author, date }) => (
    <div style={STYLES.container}>
        <span style={STYLES.author}>{author} </span>
        <span style={STYLES.date}>{new Date(date).toLocaleTimeString()}</span>
    </div>
);

ChatMessageHeader.propTypes = {
    author: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired
};

export default ChatMessageHeader;
