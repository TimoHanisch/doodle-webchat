import React from 'react';
import { shallow } from 'enzyme';
import ChoseUsername from './ChoseUsername';

describe('Test ChoseUsername', () => {
    test('should render textfield and a button', () => {
        const connecting = false;
        const onSubmit = jest.fn();
        const wrapper = shallow(
            <ChoseUsername connecting={connecting} onSubmit={onSubmit} />
        );

        expect(wrapper.childAt(0).is('TextField')).toBeTruthy();
        expect(
            wrapper
                .childAt(1)
                .dive()
                .is('Button')
        ).toBeTruthy();
    });

    test('textfield value should update if the name state changes', () => {
        const connecting = false;
        const onSubmit = jest.fn();
        const wrapper = shallow(
            <ChoseUsername connecting={connecting} onSubmit={onSubmit} />
        );

        expect(wrapper.childAt(0).props().value).toBe('');

        wrapper.setState({ name: 'test' });
        expect(wrapper.childAt(0).props().value).toBe('test');
    });

    test('should call on change if value is changed in textfield', () => {
        const connecting = false;
        const onSubmit = jest.fn();
        const eventMock = {
            target: {
                value: 'test'
            }
        };
        const wrapper = shallow(
            <ChoseUsername connecting={connecting} onSubmit={onSubmit} />
        );

        wrapper.childAt(0).simulate('change', eventMock);

        expect(wrapper.childAt(0).props().value).toBe('test');
    });

    test('should call onSubmit if button is clicked and set error to true with empty name', () => {
        const connecting = false;
        const onSubmit = jest.fn();
        const wrapper = shallow(
            <ChoseUsername connecting={connecting} onSubmit={onSubmit} />
        );

        wrapper.childAt(1).simulate('click');

        expect(wrapper.state().error).toBe(true);
    });

    test('should call onSubmit prop if name is not empty', () => {
        const connecting = false;
        const onSubmit = jest.fn();
        const wrapper = shallow(
            <ChoseUsername connecting={connecting} onSubmit={onSubmit} />
        );

        wrapper.setState({
            name: 'test'
        });

        wrapper.childAt(1).simulate('click');

        expect(onSubmit).toHaveBeenCalledTimes(1);
    });
});
