import React from 'react';
import ChatInput from './ChatInput';
import { shallow } from 'enzyme';

describe('Test ChatInput', () => {
    test('should render a form by default', () => {
        const author = 'Test';
        const sendMessage = jest.fn();
        const wrapper = shallow(
            <ChatInput localAuthor={author} sendMessage={sendMessage} />
        );

        expect(wrapper.is('div')).toBeTruthy();
        expect(wrapper.find('form')).toHaveLength(1);
    });

    test('should execute onSubmit if form is submitted', () => {
        const onSubmitSpy = jest.spyOn(ChatInput.prototype, 'onSubmit');
        const eventMock = {
            preventDefault: jest.fn()
        };
        const author = 'Test';
        const message = 'Test message';
        const sendMessage = jest.fn();
        const wrapper = shallow(
            <ChatInput localAuthor={author} sendMessage={sendMessage} />
        );
        wrapper.setState({
            message
        });
        wrapper.childAt(0).simulate('submit', eventMock);

        expect(onSubmitSpy).toHaveBeenCalledTimes(1);
        expect(eventMock.preventDefault).toHaveBeenCalledTimes(1);
        expect(sendMessage).toHaveBeenCalledWith({
            author,
            message
        });

        onSubmitSpy.mockReset();
        onSubmitSpy.mockRestore();
    });
});
