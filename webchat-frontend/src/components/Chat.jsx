import React from 'react';
import PropTypes from 'prop-types';
import ChatList from './ChatList';
import ChatInput from './ChatInput';
import { MessagePropTypes } from '../constants/propTypes';

const Chat = ({ messages, localAuthor, sendMessage }) => (
    <React.Fragment>
        <ChatList messages={messages} localAuthor={localAuthor} />
        <ChatInput localAuthor={localAuthor} sendMessage={sendMessage} />
    </React.Fragment>
);

Chat.propTypes = {
    messages: PropTypes.arrayOf(MessagePropTypes).isRequired,
    localAuthor: PropTypes.string.isRequired,
    sendMessage: PropTypes.func.isRequired
};

export default Chat;
