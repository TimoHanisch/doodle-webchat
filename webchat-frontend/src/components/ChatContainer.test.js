import React from 'react';
import { shallow } from 'enzyme';
import { ChatContainerTest } from './ChatContainer';

describe('Test ChatContainer', () => {
    test('should render username input by default', () => {
        const wrapper = shallow(<ChatContainerTest />);

        expect(wrapper.is('ChoseUsername')).toBeTruthy();
    });

    test('should render chose username if author is set, but WebSocket is still connecting', () => {
        const wrapper = shallow(
            <ChatContainerTest author="Test" wsConnecting />
        );

        expect(wrapper.is('ChoseUsername')).toBeTruthy();
    });

    test('should render chat component with right props if author is set and webSocket is connected', () => {
        const author = 'Test';
        const messages = [];
        const wsSend = jest.fn();
        const wrapper = shallow(
            <ChatContainerTest
                author={author}
                messages={messages}
                wsSend={wsSend}
            />
        );

        expect(wrapper.is('Chat')).toBeTruthy();
        expect(wrapper.props().messages).toBe(messages);
        expect(wrapper.props().localAuthor).toBe(author);
        expect(wrapper.props().sendMessage).toBe(wsSend);
    });

    test('should fetch messages if username was set', () => {
        const fetchMessages = jest.fn();
        const wsConnect = jest.fn();
        const wrapper = shallow(
            <ChatContainerTest
                fetchMessages={fetchMessages}
                wsConnect={wsConnect}
            />
        );

        expect(fetchMessages).toHaveBeenCalledTimes(0);

        wrapper.setProps({ author: 'Test' });

        expect(fetchMessages).toHaveBeenCalledTimes(1);
        expect(fetchMessages).toHaveBeenCalledWith(wsConnect);
    });
});
