import React from 'react';
import PropTypes from 'prop-types';
import {
    FormControl,
    InputLabel,
    Input,
    InputAdornment,
    IconButton
} from '@material-ui/core';
import Send from '@material-ui/icons/Send';

export default class ChatInput extends React.PureComponent {
    static propTypes = {
        localAuthor: PropTypes.string.isRequired,
        sendMessage: PropTypes.func.isRequired
    };

    static STYLES = {
        container: {
            position: 'fixed',
            width: 'calc(100vw - 64px)',
            left: 32,
            bottom: 32,
            backgroundColor: '#eeeeee',
            borderRadius: 4
        },
        form: { padding: 8 },
        formControl: { width: '100%' }
    };

    state = {
        message: ''
    };

    constructor() {
        super();
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(name) {
        return event => {
            this.setState({
                [name]: event.target.value,
                error: false
            });
        };
    }

    onSubmit(event) {
        event.preventDefault();
        const { localAuthor, sendMessage } = this.props;
        const { message } = this.state;
        if (message) {
            this.setState(
                {
                    message: ''
                },
                () => {
                    sendMessage({
                        author: localAuthor,
                        message
                    });
                }
            );
        }
    }

    render() {
        return (
            <div style={ChatInput.STYLES.container}>
                <form onSubmit={this.onSubmit} style={ChatInput.STYLES.form}>
                    <FormControl style={ChatInput.STYLES.formControl}>
                        <Input
                            placeholder="Enter a message..."
                            id="adornment-message"
                            type="text"
                            value={this.state.message}
                            onChange={this.onChange('message')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="Click to send"
                                        onClick={this.onSubmit}
                                        color="primary"
                                    >
                                        <Send />
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </form>
            </div>
        );
    }
}
