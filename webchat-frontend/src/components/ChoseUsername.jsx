import React from 'react';
import PropTypes from 'prop-types';
import { Button, TextField } from '@material-ui/core';

export default class ChoseUsername extends React.PureComponent {
    static propTypes = {
        connecting: PropTypes.bool.isRequired,
        onSubmit: PropTypes.func.isRequired
    };

    static STYLES = {
        button: { maxHeight: 36 },
        container: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            maxWidth: 320
        }
    };

    state = {
        name: '',
        error: false
    };

    constructor() {
        super();
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(event) {
        this.setState({
            name: event.target.value,
            error: false
        });
    }

    onSubmit() {
        const { onSubmit } = this.props;
        const { name } = this.state;
        if (!name) {
            this.setState({ error: true });
        } else {
            onSubmit(name);
        }
    }

    render() {
        const { connecting, onSubmit } = this.props;
        return (
            <div style={ChoseUsername.STYLES.container}>
                <TextField
                    error={this.state.error}
                    id="name"
                    label={connecting ? 'Connecting' : 'Chose Name'}
                    value={this.state.name}
                    onChange={this.onChange}
                    margin="normal"
                    disabled={connecting}
                />
                <Button
                    variant="raised"
                    color="primary"
                    style={ChoseUsername.STYLES.button}
                    onClick={this.onSubmit}
                    disabled={connecting}
                >
                    Submit
                </Button>
            </div>
        );
    }
}
