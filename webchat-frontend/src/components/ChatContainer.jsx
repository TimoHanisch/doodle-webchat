import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchMessages, updateAuthor } from '../actions/chat';
import socketActions from '../actions/socket';
import ChoseUsername from './ChoseUsername';
import Chat from './Chat';
import { MessagePropTypes } from '../constants/propTypes';

class ChatContainer extends React.Component {
    static propTypes = {
        author: PropTypes.string,
        messages: PropTypes.arrayOf(MessagePropTypes),
        messagesLoading: PropTypes.bool,
        wsConnecting: PropTypes.bool,
        wsConnected: PropTypes.bool,
        fetchMessages: PropTypes.func,
        updateAuthor: PropTypes.func,
        wsConnect: PropTypes.func,
        wsDisconnect: PropTypes.func,
        wsSend: PropTypes.func
    };

    static defaultProps = {
        author: '',
        messages: [],
        messagesLoading: false,
        wsConnecting: false,
        wsConnected: false,
        fetchMessages: () => {},
        updateAuthor: () => {},
        wsConnect: () => {},
        wsDisconnect: () => {},
        wsSend: () => {}
    };

    componentWillReceiveProps(nextProps) {
        const { author, fetchMessages, wsConnect } = this.props;
        if (!author && nextProps.author) {
            fetchMessages(wsConnect);
        }
    }

    render() {
        const {
            author,
            messages,
            messagesLoading,
            fetchMessages,
            updateAuthor,
            wsConnecting,
            wsSend
        } = this.props;
        if (!author || wsConnecting) {
            return (
                <ChoseUsername
                    onSubmit={updateAuthor}
                    connecting={wsConnecting}
                />
            );
        }
        return (
            <Chat
                messages={messages}
                localAuthor={author}
                sendMessage={wsSend}
            />
        );
    }
}

// Initializes Redux for the container component.
const mapStateToProps = ({
    author,
    messages,
    messagesLoading,
    wsConnecting,
    wsConnected
}) => {
    return {
        author,
        messages,
        messagesLoading,
        wsConnecting,
        wsConnected
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchMessages: callback => dispatch(fetchMessages(callback)),
        updateAuthor: author => dispatch(updateAuthor(author)),
        wsConnect: () => dispatch(socketActions.connect()),
        wsDisconnect: () => dispatch(socketActions.disconnect()),
        wsSend: message => dispatch(socketActions.sendMessage(message))
    };
};

export { ChatContainer as ChatContainerTest };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatContainer);
