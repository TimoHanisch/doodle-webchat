import React from 'react';
import PropTypes from 'prop-types';
import { MessagePropTypes } from '../constants/propTypes';
import ChatMessageHeader from './ChatMessageHeader';

const STYLES = {
    container: sameAuthor => ({
        backgroundColor: sameAuthor ? '#e1ffdc' : '#efefef',
        padding: 8,
        borderRadius: 2,
        maxWidth: '80%',
        marginBottom: 16,
        marginTop: 16,
        marginRight: 16,
        marginLeft: sameAuthor ? '20%' : 16
    })
};

const ChatMessage = ({ message, localAuthor }) => {
    const sameAuthor = localAuthor === message.author;
    return (
        <div style={STYLES.container(sameAuthor)}>
            <ChatMessageHeader author={message.author} date={message.date} />
            <div>{message.message}</div>
        </div>
    );
};

ChatMessage.propTypes = {
    localAuthor: PropTypes.string.isRequired,
    message: MessagePropTypes.isRequired
};

export default ChatMessage;
