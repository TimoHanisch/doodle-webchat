import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import combinedReducer from '../reducers';
import logger from 'redux-logger';
import socketMiddleware from '../middleware/socketMiddlerware';

export default function setupStore(initialState) {
    return createStore(
        combinedReducer,
        initialState,
        applyMiddleware(thunk, socketMiddleware, logger)
    );
}
