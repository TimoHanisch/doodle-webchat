export function author(state = '', { type, author }) {
    switch (type) {
        case 'AUTHOR_SET':
            return author;
        default:
            return state;
    }
}
