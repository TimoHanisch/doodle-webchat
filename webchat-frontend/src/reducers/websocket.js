export function wsConnecting(state = false, { type, isConnecting }) {
    switch (type) {
        case 'WS_CONNECTING':
            return isConnecting;
        default:
            return state;
    }
}

export function wsConnected(state = false, { type, isConnected }) {
    switch (type) {
        case 'WS_CONNECTED':
            return isConnected;
        default:
            return state;
    }
}
