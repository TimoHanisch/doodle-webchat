import { combineReducers } from 'redux';
import { author } from './author';
import { messages, messagesError, messagesLoading } from './messages';
import { wsConnected, wsConnecting } from './websocket';

export default combineReducers({
    author,
    messages,
    messagesError,
    messagesLoading,
    wsConnected,
    wsConnecting
});
