/**
 * Updates the error state in the store.
 *
 * @param {boolean} state
 * @param {{type: string, messageError: boolean}} param1
 */
export function messagesError(state = false, { type, messagesError }) {
    switch (type) {
        case 'MESSAGES_ERROR':
            return messagesError;

        default:
            return state;
    }
}

/**
 * Updates the loading state in the store.
 *
 * @param {boolean} state
 * @param {{type: string, messagesLoading: boolean}} param1
 */
export function messagesLoading(state = false, { type, messagesLoading }) {
    switch (type) {
        case 'MESSAGES_LOADING':
            return messagesLoading;

        default:
            return state;
    }
}

/**
 * Updates the message list in the store.
 *
 * @param {boolean} state
 * @param {{type: string, topstories: Array.<Message>}} param1
 */
export function messages(state = [], { type, message, messages }) {
    switch (type) {
        case 'MESSAGES_FETCHED':
            return messages;
        case 'WS_ADD_MESSAGE':
            return [...state, message];
        default:
            return state;
    }
}
