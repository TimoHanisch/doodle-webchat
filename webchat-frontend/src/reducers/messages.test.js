import { messages, messagesError, messagesLoading } from './messages';

describe('Test messages reducer', () => {
    test('messagesError should return input state if type does not match MESSAGES_ERROR', () => {
        const result = messagesError(false, { type: 'TEST' });

        expect(result).toBe(false);
    });

    test('messagesError should return messagesError if type matches MESSAGES_ERROR', () => {
        const result = messagesError(false, {
            type: 'MESSAGES_ERROR',
            messagesError: true
        });

        expect(result).toBe(true);
    });

    test('messagesLoading should return state if type does not match MESSAGES_LOADING', () => {
        const result = messagesLoading(false, {
            type: 'TEST',
            messagesLoading: true
        });

        expect(result).toBe(false);
    });

    test('messagesLoading should return messagesLoading if type matches MESSAGES_LOADING', () => {
        const result = messagesLoading(false, {
            type: 'MESSAGES_LOADING',
            messagesLoading: true
        });

        expect(result).toBe(true);
    });

    test('messages should return state if type does not match MESSAGES_FETCHED or WS_ADD_MESSAGE', () => {
        const result = messages([], { type: 'TEST' });

        expect(result).toEqual([]);
    });

    test('messages should return message if type matches MESSAGES_FETCHED', () => {
        const messagesMock = [1, 2, 3, 4];
        const result = messages([], {
            type: 'MESSAGES_FETCHED',
            messages: messagesMock
        });

        expect(result).toEqual(messagesMock);
    });

    test('messages should return merged messages if type matches WS_ADD_MESSAGE', () => {
        const message = 4;
        const result = messages([1, 2, 3], {
            type: 'WS_ADD_MESSAGE',
            message
        });

        expect(result).toEqual([1, 2, 3, 4]);
    });
});
