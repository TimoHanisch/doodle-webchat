import { wsConnected, wsConnecting } from './websocket';

describe('Test websocket reducers', () => {
    test('wsConnecting should return state if type does not match WS_CONNECTING', () => {
        const result = wsConnecting(false, { type: 'TEST' });

        expect(result).toBe(false);
    });

    test('wsConnecting should return isConnecting if type matches WS_CONNECTING', () => {
        const result = wsConnecting(false, {
            type: 'WS_CONNECTING',
            isConnecting: true
        });

        expect(result).toBe(true);
    });

    test('wsConnected should return state if type does not match WS_CONNECTED', () => {
        const result = wsConnected(false, { type: 'TEST' });

        expect(result).toBe(false);
    });

    test('wsConnected should return isConnected if type matches WS_CONNECTED', () => {
        const result = wsConnected(false, {
            type: 'WS_CONNECTED',
            isConnected: true
        });

        expect(result).toBe(true);
    });
});
