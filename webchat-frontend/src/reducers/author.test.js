import { author } from './author';

describe('Test author', () => {
    test('should return input state, if type is not equal AUTHOR_SET', () => {
        const result = author('input', { type: 'TEST' });
        expect(result).toBe('input');
    });

    test('should return author, if type is equal AUTHOR_SET', () => {
        const result = author('input', { type: 'AUTHOR_SET', author: 'test' });
        expect(result).toBe('test');
    });
});
