import actions from '../actions/socket';
import SockJS from 'sockJs';
import Stomp from 'stomp';

const WS_URL = 'http://localhost:8080/webchat';

const socketMiddleware = (function() {
    let client = null;
    let socket = null;
    const onReceive = store => ({ body }) => {
        const message = JSON.parse(body);
        store.dispatch(actions.addMessage(message));
    };

    return store => next => action => {
        switch (action.type) {
            //The user wants us to connect
            case 'WS_CONNECT':
                //Send an action that shows a "connecting..." status for now
                store.dispatch(actions.connecting(true));

                socket = new SockJS(WS_URL);
                client = Stomp.over(socket);
                client.debug = null; // Disable console print

                client.connect(
                    {},
                    frame => {
                        store.dispatch(actions.connected(true));
                        store.dispatch(actions.connecting(false));
                        client.subscribe('/topic/chat', onReceive(store));
                    }
                );

                break;

            //The user wants us to disconnect
            case 'WS_DISCONNECT':
                if (client != null) {
                    client.disconnect();
                }
                client = null;

                //Set our state to disconnected
                store.dispatch(actions.connected(false));
                break;

            //Send the 'SEND_MESSAGE' action down the websocket to the server
            case 'WS_SEND_MESSAGE':
                client.send('/app/message', {}, JSON.stringify(action.message));
                break;

            //This action is irrelevant to us, pass it on to the next middleware
            default:
                return next(action);
        }
    };
})();

export default socketMiddleware;
