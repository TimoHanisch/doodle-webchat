import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ChatContainer from './components/ChatContainer';
import registerServiceWorker from './registerServiceWorker';
import setupStore from './store/store';
import './index.css';

const store = setupStore();

ReactDOM.render(
    <Provider store={store}>
        <ChatContainer />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
