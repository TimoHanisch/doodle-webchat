# Frontend

Based off an ejected React-Starter app.

## Structure

-   `/config` contains the `jest/webpack` configurations for Dev + Prod Environment
-   `/public` is used for all static assets (HTML etc.)
-   `/scripts` contains convenient scripts for starting/testing
-   `/src` contains the source and test files

## Uses

-   React/Material-UI -> View
-   Redux -> State
-   SockJS/Stomp/Fetch -> Transport
