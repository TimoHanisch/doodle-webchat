# Doodle WebChat

This repository consists of a backend and a frontend integration for a basic webchat based on WebSockets.

## Requirements

* Gradle
* Jave 8
* node.js 8+

## How to run

### Backend

Starts up the development environment.

```
$ cd ./webchat-backend
$ gradle bootRun
```

### Frontend

Starts up the development environment.

#### With yarn

```
$ cd ./webchat-frontend
$ yarn start
```

## How to run tests

### Backend

```
$ cd ./webchat-backend
$ gradle test
```

### Frontend

#### With yarn

```
$ cd ./webchat-frontend
$ yarn test
```

### Future

TODO: Use Docker for easier deployment.
